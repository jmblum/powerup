# PowerUp

## Description
The PowerUp project is a power production calculator for determining the production-plan regarding how much power each of a multitude of different powerplants need to produce. It takes into account the loads, the cost of the underlying energy sources (gas, kerosine), and the Pmin and Pmax of each powerplant.

See [Main Requirement Documentation](./specifications/main_requirements.md) for more details.

## Build and Launch Instructions

### Run locally in debug mode run:
```
cd ./powerup
bash ./create_virtualenv.sh
uvicorn main:app --reload --port 8888

```
Open a browser to http://localhost:8888/docs and/or http://localhost:8888/redoc to view a UI and usage

### Run with Docker
`TODO`, not yet implemented

## Testing
```
cd ./powerup
pytest
```

## Roadmap
0. [X] Complete research, clarify specifications, roadmap, project planning
1. [X] feature_calculate_power_requirements : Scenario without CO2
	- 3hr for code
	- limited tests
2. [X] feature_expose_productionplan_post_API.feature : Scenario for valid input
	- 20 min for code
	- limited tests
6. [X] feature_docs.feature
	- 20 min inc. verification checks of process, and explanation of what is 
-------------------------------
Incomplete:	
3. [-] feature_return_response_JSON_payload.feature
	- 20 min for code
	- limited tests
	- Output format is not correct
4. [ ] feature_handle_production_plan_input_payload.feature : Scenario for valid data
	- 20 min for code
	- limited tests
5. [ ] feature_Dockerise_Solution.feature
	- 20 minincomplete and why
8. [ ] Integrate tests, etc. with CI/CD
9. [ ] feature_calculate_power_requirements : Scenario with CO2
10. [ ] feature_expose_productionplan_post_API.feature : Scenario for invalid input
11.[ ] feature_handle_production_plan_input_payload.feature : Scenario for invalid data
12. [ ] Refactor code/test/docs not. inc. calculation algorithm
13. [ ] Refactor calculation algorithm