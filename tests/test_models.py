""" Data model tests for the PowerUp application
    Note: Test cases only include happy paths for now owing to lack of time
"""
from src.models import (
    Load,
    Fuel,
    Gas_Fuel,
    Kerosine_Fuel,
    CO2_Fuel,
    Wind_Fuel,
    Powerplant,
    Windturbine_Powerplant,
    Simple_Gasfired_Powerplant,
    CO2_Gasfired_Powerplant,
    Turbojet_Powerplant,
)


class TestLoadModels:
    def prepare_Load(self):
        return Load(**{"target": "480", "current": "0"})

    def test_model_Load(self):
        assert self.prepare_Load().dict() == {"target": 480, "current": 0}


class TestFuelModels:
    def prepare_Fuel(self):
        return Fuel(**{"description": "gas(euro/MWh)", "value": "13.4"})

    def test_model_Fuel(self):
        assert self.prepare_Fuel().dict() == {
            "description": "gas(euro/MWh)",
            "value": 13.4,
        }

    def prepare_Gas_Fuel(self):
        return Gas_Fuel(**{"value": "13.4"})

    def test_model_Gas_Fuel(self):
        assert self.prepare_Gas_Fuel().dict() == {
            "description": "gas(euro/MWh)",
            "value": 13.4,
        }

    def prepare_Kerosine_Fuel(self):
        return Kerosine_Fuel(**{"value": "50.8"})

    def test_model_Kerosine_Fuel(self):
        assert self.prepare_Kerosine_Fuel().dict() == {
            "description": "kerosine(euro/Mwh)",
            "value": 50.8,
        }

    def prepare_CO2_Fuel(self):
        return CO2_Fuel(**{"value": "20"})

    def test_model_CO2_Fuel(self):
        assert self.prepare_CO2_Fuel().dict() == {
            "description": "co2(euro/ton)",
            "value": 20,
        }

    def prepare_Wind_Fuel(self):
        return Wind_Fuel(**{"value": "60"})

    def test_model_Wind_Fuel(self):
        assert self.prepare_Wind_Fuel().dict() == {
            "description": "wind(%)",
            "value": 60,
        }


class TestPowerplantModels:
    def prepare_Powerplant(self):
        powerplant_data = {
            "name": "unknown",
            "type": "unknown",
            "efficiency": "0.53",
            "pmin": "100",
            "pmax": "460",
            "available": "-1",
            "fuel_conditions": [],
            "expected_power": "-1",
            "cost_expected_power": "-1",
        }
        return Powerplant(**powerplant_data)

    def test_model_Powerplant(self):
        assert self.prepare_Powerplant().dict() == {
            "name": "unknown",
            "type": "unknown",
            "efficiency": 0.53,
            "pmin": 100,
            "pmax": 460,
            "available": -1,
            "fuel_conditions": [],
            "expected_power": -1,
            "cost_expected_power": -1,
        }

    def prepare_simple_Gasfired_Powerplant(self):
        powerplant_data = {
            "name": "gasfiredbig1",
            "efficiency": "0.53",
            "pmin": "100",
            "pmax": "460",
            "available": "-1",
            "expected_power": "-1",
            "cost_expected_power": "-1",
        }
        return Simple_Gasfired_Powerplant(**powerplant_data)

    def prepare_cO2_Gasfired_Powerplant(self):
        powerplant_data = {
            "name": "gasfiredbig1",
            "efficiency": "0.53",
            "pmin": "100",
            "pmax": "460",
            "available": "-1",
            "expected_power": "-1",
            "cost_expected_power": "-1",
        }
        return CO2_Gasfired_Powerplant(**powerplant_data)

    def prepare_turbojet_Powerplant(self):
        powerplant_data = {
            "name": "tj1",
            "efficiency": "0.3",
            "pmin": "0",
            "pmax": "16",
            "available": "-1",
            "expected_power": "-1",
            "cost_expected_power": "-1",
        }
        return Turbojet_Powerplant(**powerplant_data)

    def prepare_windturbine_Powerplant(self):
        powerplant_data = {
            "name": "windpark1",
            "efficiency": "1",
            "pmin": "0",
            "pmax": "150",
            "available": "-1",
            "expected_power": "-1",
            "cost_expected_power": "-1",
        }
        return Windturbine_Powerplant(**powerplant_data)

    def test_model_simple_Gasfired_Powerplant(self):
        assert self.prepare_simple_Gasfired_Powerplant().dict() == {
            "name": "gasfiredbig1",
            "type": "gasfired",
            "efficiency": 0.53,
            "pmin": 100,
            "pmax": 460,
            "fuel_conditions": ["Gas_Fuel"],
            "available": -1,
            "expected_power": -1,
            "cost_expected_power": -1,
        }

    def test_model_cO2_Gasfired_Powerplant(self):
        assert self.prepare_cO2_Gasfired_Powerplant().dict() == {
            "name": "gasfiredbig1",
            "type": "gasfired",
            "efficiency": 0.53,
            "pmin": 100,
            "pmax": 460,
            "fuel_conditions": ["Gas_Fuel", "CO2_Fuel"],
            "available": -1,
            "expected_power": -1,
            "cost_expected_power": -1,
        }

    def test_model_turbojet_Powerplant(self):
        assert self.prepare_turbojet_Powerplant().dict() == {
            "name": "tj1",
            "type": "turbojet",
            "efficiency": 0.3,
            "pmin": 0,
            "pmax": 16,
            "fuel_conditions": ["Kerosine_Fuel"],
            "available": -1,
            "expected_power": -1,
            "cost_expected_power": -1,
        }

    def test_model_windturbine_Powerplant(self):
        assert self.prepare_windturbine_Powerplant().dict() == {
            "name": "windpark1",
            "type": "windturbine",
            "efficiency": 1,
            "pmin": 0,
            "pmax": 150,
            "fuel_conditions": ["Wind_Fuel"],
            "available": -1,
            "expected_power": -1,
            "cost_expected_power": -1,
        }
