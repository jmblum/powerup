import logging
import json
from jsonschema import Draft4Validator
from src.models import (
    Load,
    Fuel,
    Gas_Fuel,
    Kerosine_Fuel,
    CO2_Fuel,
    Wind_Fuel,
    Powerplant,
    Windturbine_Powerplant,
    Simple_Gasfired_Powerplant,
    CO2_Gasfired_Powerplant,
    Turbojet_Powerplant
)
import src.calculations
from fastapi import (FastAPI, File, UploadFile)
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder

logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)
app = FastAPI()

@app.post("/productionplan")
def postProductionplan(upload_file: UploadFile = File(...)):
    json_data = json.load(upload_file.file)
    schema = get_schema()
    # Note this is not working but ran out of time, so always presuming good data
    if False == Draft4Validator(schema).is_valid(json_data):
        logging.info("invalid input...")
        return JSONResponse(
            status_code=422,
            content=jsonable_encoder({"error": "Invalid input file."}),
        )
    results = src.calculations.determine_production_plan(json_data["load"], json_data["fuels"], json_data["powerplants"])
    if results["status"] != "200_OK":
        return JSONResponse(
            status_code=422,
            content=jsonable_encoder({"error": "Invalid input file."}),
        )
    logging.info(results)
    return output_results(results["powerplants"])

def get_schema():
    """This function loads the given schema available"""
    with open('./specifications/inferred_schemas/productionplan_payload.json', 'r') as file:
        schema = json.load(file)
    return schema
def output_results(powerplants):
    """ Not implemented but the idea is to format each of the results["powerplants"] entities into {"name": aPowerplant["name"], "p": aPowerplant["expected_power"]}
    """
    return powerplants


    
    