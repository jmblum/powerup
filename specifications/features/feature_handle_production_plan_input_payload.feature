Feature: Handle productionplan input data
  Scenario: Eugene posts valid productionplan input
    Given a valid JSON productionplan input payload
    When I validate the record against the ./inferred_schemas/productionplan_payload.json
    Then the payload should be enqueued in the calculation queue

  Scenario: Eugene posts invalid productionplan input
    Given an invalid JSON productionplan input payload
    When I validate the record against the ./inferred_schemas/productionplan_payload.json
    Then return a 400 (Bad Request) with a message explaining the issue
    And the input and reason should be logged to an errors.txt log file