Feature: Calculate how much power each of the powerplants need to produce
  Scenario Outline: Calculation based on load, list of powerplants and list of fuels
    Given <load>, <list_of_powerplants>, and <list_of_fuels>
    And The power produced by each powerplant has to be a multiple of 0.1 Mw
    And The sum of the power produced by all the powerplants together should equal the load
    When I calculate powerplants power produce needs
    Then I should have <output> values
    And the calculation must be from an algorithm written from scratch

    Examples:
    | load | list_of_powerplants | list_of_fuels | output |
    |  480   |  [ {  "name": "gasfiredbig1",  "type": "gasfired",  "efficiency": 0.53,  "pmin": 100,  "pmax": 460 }, {  "name": "gasfiredbig2",  "type": "gasfired",  "efficiency": 0.53,  "pmin": 100,  "pmax": 460 }, {  "name": "gasfiredsomewhatsmaller",  "type": "gasfired",  "efficiency": 0.37,  "pmin": 40,  "pmax": 210 }, {  "name": "tj1",  "type": "turbojet",  "efficiency": 0.3,  "pmin": 0,  "pmax": 16 }, {  "name": "windpark1",  "type": "windturbine",  "efficiency": 1,  "pmin": 0,  "pmax": 150 }, {  "name": "windpark2",  "type": "windturbine",  "efficiency": 1,  "pmin": 0,  "pmax": 36 } ]  |    {"gas(euro/MWh)": 13.4, "kerosine(euro/MWh)": 50.8, "co2(euro/ton)": 20, "wind(%)": 60} | TODO -- Work this out! |
    TODO add more examples...

  Scenario Outline: Calculation based on load, list of powerplants, list of fuels, and CO2
    Given <load>, <list_of_powerplants>, and <list_of_fuels>        
    And The power produced by each powerplant has to be a multiple of 0.1 Mw
    And The sum of the power produced by all the powerplants together should equal the load
    And the cost of running the powerplant should also take into account the cost of the emission allowances
    And Each MWh generated from gas-fired powerplant creates 0.3 ton of CO2 for the sake of this project
    When I calculate powerplants power produce needs
    Then I should have <output> values

    Examples:
    | load | list_of_powerplants | list_of_fuels | output |
    TODO add examples...