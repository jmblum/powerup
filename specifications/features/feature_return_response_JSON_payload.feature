Feature: Return a valid response JSON payload containing per powerplant records of how much power each powerplant should deliver 
  Scenario Outline: Eugene posts valid productionplan input
    Given the JSON_Schema: ./inferred_schemas/productionplan_output_payload.json
    And <calculation_output> values based on the input
    When I provide the API POST response
    Then the API POST response should contain the <payload>

    Examples:
    | calculation_output | payload |
    |TODO WORK THIS OUT | [  { "name": "windpark1", "p": 75  },  { "name": "windpark2", "p": 18  },  { "name": "gasfiredbig1", "p": 200  },  { "name": "gasfiredbig1", "p": 0  },  { "name": "tj1", "p": 0  },  { "name": "tj2", "p": 0  }] |
    TODO add more examples...