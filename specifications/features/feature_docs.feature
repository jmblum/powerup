Feature: Provide a README.md explaining how to build and launch the API
  Scenario: Allan the admin wants to get up and running quickly with the solution 
    Given the code repo contains a README.md explaining how to build and launch the API
    When Allan clones the repo and reads the README
    Then he can build and launch the API easily