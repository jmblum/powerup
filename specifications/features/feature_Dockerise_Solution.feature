Feature: Provide a Dockerfile along with the implementation to allow deploying your solution quickly
  Scenario: Allan the admin wants to get up and running wquickly with the solution 
    Given the code repo contains a correct Dockerfile
    When Allan runs docker
    Then the correct solution is up and running in the containerised environment