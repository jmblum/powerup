Feature: Provide a REST API that has a productionplan POST method
  Scenario: Eugene posts valid productionplan input data to <URL>:8888/productionplan 
    Given a valid JSON productionplan input payload
    When I receive the data
    Then I return HTTP Status 200 (OK) and a JSON payload

  Scenario: Eugene posts invalid data to <URL>:8888/productionplan  
    Given an invalid JSON productionplan input payload
    When I receive the data
    Then I return a 400 (Bad Request) with a message explaining the issue