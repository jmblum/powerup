---
tags:
- object_type-cognition/knowledge/business_glossary
- object_type-cognition/knowledge/taxonomy
---

> [!INFO] # Business concepts and other useful info related to the business in response to the [GEM](https://gems.engie.com/) [powerplant-coding-challenge](https://github.com/gem-spaas/powerplant-coding-challenge)
> * LoadDate::[[2022-09-22]] 15:40

> [!INFO] # Body

> [!abstract] ## Description
- [GEM](https://gems.engie.com/) -- 'Global Energy Management'
	+ energy management arm of [ENGIE](https://www.engie.com/)
	+ one of the largest global energy players
	+ access to local markets all over the world
- IS team of the 'Short-term Power as-a-Service'
	+ 100 people with experiencein:
		* energy markets
		* IT
		* modeling
	+ [day-ahead](https://en.wikipedia.org/wiki/European_Power_Exchange#Day-ahead_markets) market
	+ [intraday markets](https://en.wikipedia.org/wiki/European_Power_Exchange#Intraday_markets) 
	+ [collaborate with the TSO to balance the grid continuously](https://en.wikipedia.org/wiki/Transmission_system_operator#Electricity_market_operations)
- Load
	+ The load is the continuous demand of power. 
	+ The total load at each moment in time is forecasted. 
	+ For instance for Belgium you can see the load forecasted by the grid operator [here](https://www.elia.be/en/grid-data/load-and-load-forecasts).
	+ All available powerplants need to generate the power to exactly match the load at all times
	+ The cost of generating power per powerplant depends on external factors. E.g.: 
		* The cost of producing power using a [turbojet](https://en.wikipedia.org/wiki/Gas_turbine#Industrial_gas_turbines_for_power_generation)
		that runs on kerosine is higher 
			- [thermal efficiency](https://en.wikipedia.org/wiki/Thermal_efficiency) of a turbojet is around 30%
		* compared to the cost of generating power using a gas-fired powerplant 
			- because of gas being cheaper compared to kerosine
			- and because more thermal efficient
				+ thermal efficiency of a gas-fired powerplant is around 50% 	
					* 2 units of gas will generate 1 unit of electricity
		The cost of generating power using windmills however is zero
	+ Deciding which powerplants to activate is dependent on the [merit-order](https://en.wikipedia.org/wiki/Merit_order).
		* a.k.a. [unit-commitment problem](https://en.wikipedia.org/wiki/Unit_commitment_problem_in_electrical_power_production)
			- the maximum amount of power each powerplant can produce (Pmax) needs to be taken into account
			- gas-fired powerplants generate a certain minimum amount of power when switched on, called the Pmin


> [!INFO] # Back Matter

> [!example] ## References

-   [Global Energy Management Solutions](https://www.youtube.com/watch?v=SAop0RSGdHM)
-   [COO hydroelectric power station](https://www.youtube.com/watch?v=edamsBppnlg)
-   [Management of supply](https://www.youtube.com/watch?v=eh6IIQeeX3c) - video made during winter 2018-2019