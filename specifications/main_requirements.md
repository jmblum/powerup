---
tags:
- object_type-IT/development/specification
---

> [!INFO] # Main requirements document for PowerUp, a response to the [GEM](https://gems.engie.com/) [powerplant-coding-challenge](https://github.com/gem-spaas/powerplant-coding-challenge)
> * LoadDate::[[2022-09-22]] 15:26

> [!INFO] # Body

> [!abstract] ## Description
> The following translates the key requirements/specification points from the powerplant-coding-challenge README into pytest-bdd flavoured subset of the [Gherkin language](https://cucumber.io/docs/gherkin/). See [business glossary)[./business_glossary.md] for useful info.

Overall Background:
	- This project is a response to the GEM coding challenge. 
	- Purpose: provide some insight into the business
	- Incomplete submission is OK
	- ??? Submitting a pull-request will not automatically trigger the recruitement process. ??? -- Does this mean I need to use their repo?
	- Functional Objectives
		0. Given a composite record containing:
			1. a load value
			2. a list of powerplants
			3. a list of fuels
		1. Validate the record and handle errors appropriately 
			+ further described in ./specifications/feature_handle_production_plan_input_payload.txt
		2. Given a valid record: Calculate how much power each of the powerplants need to produce (a.k.a. the production-plan) taking into account the cost of the underlying energy sources (gas, kerosine) and the Pmin and Pmax of each powerplant.
			+ The power produced by each powerplant has to be a multiple of 0.1 Mw 
			+ The sum of the power produced by all the powerplants together should equal the load
			+ further described in ./specifications/feature_calculate_power_requirements.txt
		3. Return a response JSON payload containing per powerplant records of how much power each powerplant should deliver in the format from the to be inferred from the ./example_input/example_payloads/* (./inferred_schemas/productionplan_output_payload.json)
			+ further described in ./specifications/feature_return_response_JSON payload.txt
		- Optional 4.
			Taken into account that a gas-fired powerplant also emits CO2, the cost of running the powerplant should also take into account the cost of the emission allowances.
				+ Each MWh generated from gas-fired powerplant creates 0.3 ton of CO2 for the sake of this project
				+ further described in ./specifications/feature_calculate_power_requirements.txt
	- Non-functional requirements
		+ Implementation
			* REST API exposing an endpoint /productionplan
				- accepts a POST with a payload as per the schema to be inferred from the ./example_input/example_payloads/* (./inferred_schemas/productionplan_payload.json)
				- expose the API on port 8888
				- returns a valid json as per Functional Objective 3
			* Manage and log run-time errors
			* Implementations must not rely on an external solver and thus contain an algorithm written from scratch
		+ Deployment
			* Provide a Dockerfile along with the implementation to allow deploying your solution quickly.
		+ Operation
			* Contains a README.md explaining how to build and launch the API

> [!INFO] # Back Matter

> [!example] ## References
