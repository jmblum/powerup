""" Calculation functions for the PowerUp application
"""
from src.models import (
    Load,
    Fuel,
    Gas_Fuel,
    Kerosine_Fuel,
    CO2_Fuel,
    Wind_Fuel,
    Powerplant,
    Windturbine_Powerplant,
    Simple_Gasfired_Powerplant,
    CO2_Gasfired_Powerplant,
    Turbojet_Powerplant,
)
import logging


def determine_powerplant_cost(powerplant: Powerplant, fuel: [Fuel]):
    """Calculate the powerplant cost
    :field Powerplant powerplant: The given powerplant for which to calculate the costs
    :field [Fuel] fuels: Fuel details
    :return Powerplant
	"""
    logging.info("determine_powerplant_cost...")
    
    if powerplant["type"] == "windturbine":
        return determine_powerplant_cost_and_availability_windturbine(powerplant, fuel)
    elif powerplant["type"] == "turbojet":
        return determine_powerplant_cost_and_availability_turbojet(powerplant, fuel)
    elif powerplant["type"] == "gasfired":
        return determine_powerplant_cost_and_availability_simple_Gasfired(
            powerplant, fuel
        )
    # TODO: CO2_Gasfired_Powerplant
    logging.info("determine_powerplant_cost complete.")


def determine_powerplant_cost_and_availability_windturbine(
    powerplant: Powerplant, fuel: [Fuel]
):
    """Calculate the windturbine cost and availability
    :field Powerplant powerplant: The given powerplant for which to calculate the costs
    :field [Fuel] fuels: Fuel details
    :return Powerplant or None if problems with input
	"""
    afuel = getAFuelFromFuels(Wind_Fuel(**{"value": "0"}).description, fuel)
    if afuel == None:
        return None
    powerplant["available"] = powerplant["pmax"] * (afuel[1] / 100)
    powerplant["cost_expected_power"] = 0
    return powerplant


def determine_powerplant_cost_and_availability_turbojet(
    powerplant: Powerplant, fuel: [Fuel]
):
    """Calculate the turbojet cost and availability
    :field Powerplant powerplant: The given powerplant for which to calculate the costs
    :field [Fuel] fuels: Fuel details
    :return Powerplant or None if problems with input
	"""
    afuel = getAFuelFromFuels(Kerosine_Fuel(**{"value": "0"}).description, fuel)
    if afuel == None:
        return None
    powerplant["available"] = powerplant["pmax"]
    powerplant["cost_expected_power"] = round(afuel[1] / powerplant.efficiency)
    return powerplant


def determine_powerplant_cost_and_availability_simple_Gasfired(
    powerplant: Powerplant, fuel: [Fuel]
):
    """Calculate the simple_Gasfired cost and availability
    :field Powerplant powerplant: The given powerplant for which to calculate the costs
    :field [Fuel] fuels: Fuel details
    :return Powerplant or None if problems with input
	"""
    afuel = getAFuelFromFuels(Gas_Fuel(**{"value": "0"}).description, fuel)
    if afuel == None:
        logging.warning("Fuel not found. Defaulting to cost of 1000")
        fuel_val = 1000
    else:
        fuel_val = afuel[1]
    powerplant["available"] = powerplant["pmax"]
    powerplant["cost_expected_power"] = round(fuel_val / powerplant["efficiency"])
    return powerplant


def determine_powerplant_cost_and_availability_co2_Gasfired(
    powerplant: Powerplant, fuel: [Fuel]
):
    """Calculate the co2_Gasfired cost and availability
    :field Powerplant powerplant: The given powerplant for which to calculate the costs
    :field [Fuel] fuels: Fuel details
    :return None as this is not yet implemented
	"""
    return None


def getAFuelFromFuels(descriptionIn, fuels):
    """Returns the first fuel of interest in a list of fuels
	"""
    fuelList = list(fuels.items())
    i = 0
    while i < len(fuelList):
        if fuelList[i][0] == descriptionIn:
            return fuelList[i]
        i = i + 1
    return None

def determine_production_plan(load: Load, fuels: [Fuel], powerplants: [Powerplant]):
    """The main calculation returns the production-plan containing the amount of power per powerplant needed to be produced given a particular load. The power produced by each powerplant has to be a multiple of 0.1 Mw and the sum of the power produced by all the powerplants together should equal the load.
    :field Load load: See models.Load
    :field [Fuel] fuels: See models.Fuel
    :field [Powerplant] powerplants: See models.Powerplant
    :return {'status':['OK'|ERROR_***INFORMATION***'], 'powerplants': [Powerplant]}
	"""
    logging.info("determine_production_plan...")
    powerSum = 0
    return_powerplants = []

    # Calculate the costs and availabilities of the powerplants
    i = 0
    while i < len(powerplants):
        aPowerplant = powerplants[i]
        if aPowerplant == None:
            continue

        bPowerplant = determine_powerplant_cost(aPowerplant, fuels)
        if bPowerplant == None:
            logging.warning("Could not determine powerplant cost for: " + aPowerplant["name"])
        else:
            powerplants[i] = bPowerplant
        i = i + 1
    # Order powerplants by cost
    #logging.info("powerplants")
    #logging.info(powerplants)
    powerplants.sort(key=lambda item: str(item.get("cost_expected_power")), reverse=False)
    #logging.info("sorted_powerplants")
    #logging.info(powerplants)

    # # ---- NOTE: This algorithm does not handle the condition that X then has a pMin that is not satisfied -- to fix this keep track of the pop stack but ran out of time to implement this so keeping it simple -- possible to have infinite loop -- TODO correct this if time
    
    while powerSum < load and len(powerplants) != 0:
        aPowerplant = powerplants.pop(0)

        if aPowerplant["pmin"] + powerSum > load:
            powerplants = [return_powerplants.pop()] + powerplants
            powerplants = [aPowerplant] + powerplants

        elif load - powerSum >= aPowerplant["available"]:
            aPowerplant["expected_power"] = aPowerplant["available"]
            powerSum = powerSum + aPowerplant["expected_power"]
            return_powerplants.append(aPowerplant)

        elif load - powerSum < aPowerplant["available"]:
            aPowerplant["expected_power"] = load - powerSum
            powerSum = powerSum + aPowerplant["expected_power"]
            return_powerplants.append(aPowerplant)

    if powerSum < load:
        logging.warning("The powerSum < load!")
        return {
            "status": "422_ERROR_Unprocessable-Entity",
            "target_load": load,
            "returned_load": powerSum,
            "powerplants": return_powerplants,
        }
    else:
        return {
            "status": "200_OK",
            "target_load": load,
            "returned_load": powerSum,
            "powerplants": return_powerplants,
        }


if __name__ == "__main__":
    logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

    load = Load(**{"target": "480", "current": "0"})
    fuels = [
        Gas_Fuel(**{"value": "13.4"}),
        Kerosine_Fuel(**{"value": "50.8"}),
        CO2_Fuel(**{"value": "20"}),
        Wind_Fuel(**{"value": "60"}),
    ]
    powerplants = [
        Simple_Gasfired_Powerplant(
            **{
                "name": "gasfiredbig1",
                "efficiency": "0.53",
                "pmin": "100",
                "pmax": "460",
                "available": "-1",
                "expected_power": "-1",
                "cost_expected_power": "-1",
            }
        ),
        Turbojet_Powerplant(
            **{
                "name": "tj1",
                "efficiency": "0.3",
                "pmin": "0",
                "pmax": "16",
                "available": "-1",
                "expected_power": "-1",
                "cost_expected_power": "-1",
            }
        ),
        Windturbine_Powerplant(
            **{
                "name": "windpark1",
                "efficiency": "1",
                "pmin": "0",
                "pmax": "150",
                "available": "-1",
                "expected_power": "-1",
                "cost_expected_power": "-1",
            }
        ),
    ]
    print(determine_production_plan(load, fuels, powerplants))
