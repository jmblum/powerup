""" Data models for the PowerUp application
"""

from typing import Optional
from pydantic import BaseModel


class Load(BaseModel):
    """The load is the continuous demand of power. The total load at each moment in time is forecasted.
    :field int target: The final expected load
    :field int current: The current total load for a calculation
    """

    target: int
    current: int


class Fuel(BaseModel):
    """A type of powerplant fuel
    :field str description: Description of factor influening price calculation
    :field float value: value for the described factor
    """

    description: str
    value: float


class Gas_Fuel(Fuel):
    """the price of gas per MWh. Thus if gas is at 6 euro/MWh and if the efficiency of the powerplant is 50% (i.e. 2 units of gas will generate one unit of electricity), the cost of generating 1 MWh is 12 euro.
    """

    description = "gas(euro/MWh)"
    value: float


class Kerosine_Fuel(Fuel):
    """ the price of kerosine per MWh.
    """

    description = "kerosine(euro/Mwh)"
    value: float


class CO2_Fuel(Fuel):
    """ the price of emission allowances 
    """

    description = "co2(euro/ton)"
    value: float


class Wind_Fuel(Fuel):
    """ percentage of wind. Example: if there is on average 25% wind during an hour, a wind-turbine with a Pmax of 4 MW will generate 1MWh of energy.
    """

    description = "wind(%)"
    value: float


class Powerplant(BaseModel):
    """ describes the powerplants at disposal to generate the demanded load.
    :field str name: Id of the particular powerplant
    :field str type: one of -- gasfired, turbojet or windturbine.
    :field float efficiency: the efficiency at which they convert a MWh of fuel into a MWh of electrical energy. Wind-turbines do not consume 'fuel' and thus are considered to generate power at zero price.
    :field int pmin: the maximum amount of power the powerplant can generate.
    :field int pmax: the minimum amount of power the powerplant generates when switched on.
    :field int available: Calculated value that uses either pmax or some % of it (e.g. for wind). If negative value then it is unknown.
    :field list[str] fuel_conditions: Which of the Fuel types (can be more than one -- e.g. C02 and Gas for gasfired -- to take into account when calculating load)
    :field float expected_power: Calculated value of how much power each powerplant should deliver. The power produced by each powerplant has to be a multiple of 0.1 Mw.  If negative value then it is unknown.
    :field float cost_expected_power: Calculated value of euro/MWh (rounded to Euro) given fuel cost and efficiency. If negative value then it is unknown.
    """

    name: str
    type: str
    efficiency: float
    pmin: int
    pmax: int
    available: int
    fuel_conditions: list[str]
    expected_power: float
    cost_expected_power: int


class Simple_Gasfired_Powerplant(Powerplant):
    name: str
    type = "gasfired"
    efficiency: float
    pmin: int
    pmax: int
    available: int
    fuel_conditions: list[str] = ["Gas_Fuel"]
    expected_power: float
    cost_expected_power: int


class CO2_Gasfired_Powerplant(Powerplant):
    name: str
    type = "gasfired"
    efficiency: float
    pmin: int
    pmax: int
    available: int
    fuel_conditions: list[str] = ["Gas_Fuel", "CO2_Fuel"]
    expected_power: float
    cost_expected_power: int


class Turbojet_Powerplant(Powerplant):
    name: str
    type = "turbojet"
    efficiency: float
    pmin: int
    pmax: int
    available: int
    fuel_conditions: list[str] = ["Kerosine_Fuel"]
    expected_power: float
    cost_expected_power: int


class Windturbine_Powerplant(Powerplant):
    name: str
    type = "windturbine"
    efficiency: float
    pmin: int
    pmax: int
    available: int
    fuel_conditions: list[str] = ["Wind_Fuel"]
    expected_power: float
    cost_expected_power: int


if __name__ == "__main__":
    load_data = {"target": "480", "current": "0"}

    load = Load(**load_data)
    print(load.target)
    # > 480
    print(load.dict())
    """
    {
        'id': 480,
        'current': 0
    }
    """

    fuel_data = {"description": "gas(euro/MWh)", "value": "13.4"}

    fuel = Fuel(**fuel_data)
    print(fuel.value)
    # > 13.4
    print(fuel.dict())
    """
    {
        'description': 'gas(euro/MWh)',
        'value': '13.4'
    }
    """

    gas_fuel_data = {"value": "13.4"}

    gas_fuel = Gas_Fuel(**gas_fuel_data)
    print(gas_fuel.description)
    # > gas(euro/MWh)
    print(gas_fuel.dict())
    """
    {
        'description': 'gas(euro/MWh)',
        'value': '13.4'
    }
    """

    kerosine_fuel_data = {"value": "50.8"}

    kerosine_fuel = Kerosine_Fuel(**kerosine_fuel_data)
    print(kerosine_fuel.description)
    # > kerosine(euro/Mwh)
    print(kerosine_fuel.dict())
    """
    {
        'description': 'kerosine(euro/Mwh)',
        'value': '50.8'
    }
    """

    cO2_fuel_data = {"value": "20"}

    cO2_fuel = CO2_Fuel(**cO2_fuel_data)
    print(cO2_fuel.description)
    # > co2(euro/ton)
    print(cO2_fuel.dict())
    """
    {
        'description': 'co2(euro/ton)',
        'value': '20'
    }
    """

    wind_fuel_data = {"value": "60"}

    wind_fuel = Wind_Fuel(**wind_fuel_data)
    print(wind_fuel.description)
    # > wind(%)
    print(wind_fuel.dict())
    """
    {
        'description': 'wind(%)',
        'value': '60'
    }
    """

    powerplant_data = {
        "name": "unknown",
        "type": "unknown",
        "efficiency": "0.53",
        "pmin": "100",
        "pmax": "460",
        "available": "-1",
        "fuel_conditions": [],
        "expected_power": "-1",
        "cost_expected_power": "-1",
    }

    powerplant = Powerplant(**powerplant_data)
    print(powerplant.name)
    # > gasfiredbig1
    print(powerplant.dict())
    """
    {
      'name': 'unknown',
      'type': 'unknown',
      'efficiency': '0.53',
      'pmin': '100',
      'pmax': '460',
      'available": '-1',
      'fuel_conditions': [],
      'expected_power': '-1',
      'cost_expected_power': '-1'
    }
    """
    fuel_conditions: list[str] = ["Wind_Fuel"]

    simple_Gasfired_Powerplant_data = {
        "name": "gasfiredbig1",
        "efficiency": "0.53",
        "pmin": "100",
        "pmax": "460",
        "available": "-1",
        "expected_power": "-1",
        "cost_expected_power": "-1",
    }

    simple_Gasfired_Powerplant = Simple_Gasfired_Powerplant(
        **simple_Gasfired_Powerplant_data
    )
    print(simple_Gasfired_Powerplant.name)
    # > gasfiredbig1
    print(simple_Gasfired_Powerplant.dict())
    """
    {
      'name': 'gasfiredbig1',
      'type': 'gasfired',
      'efficiency': '0.53',
      'pmin': '100',
      'pmax': '460',
      'available": '-1',
      'fuel_conditions': ['Gas_Fuel'],
      'expected_power': '-1',
      'cost_expected_power': '-1'
    }
    """

    co2_Gasfired_Powerplant_data = {
        "name": "gasfiredbig1",
        "efficiency": "0.53",
        "pmin": "100",
        "pmax": "460",
        "available": "-1",
        "expected_power": "-1",
        "cost_expected_power": "-1",
    }

    cO2_Gasfired_Powerplant = CO2_Gasfired_Powerplant(**co2_Gasfired_Powerplant_data)
    print(cO2_Gasfired_Powerplant.name)
    # > gasfiredbig1
    print(cO2_Gasfired_Powerplant.dict())
    """
    {
      'name': 'gasfiredbig1',
      'type': 'gasfired',
      'efficiency': '0.53',
      'pmin': '100',
      'pmax': '460',
      'available": '-1',
      'fuel_conditions': ['Gas_Fuel', 'CO2_Fuel'],
      'expected_power': '-1',
      'cost_expected_power': '-1'
    }
    """

    turbojet_Powerplant_data = {
        "name": "tj1",
        "efficiency": "0.3",
        "pmin": "0",
        "pmax": "16",
        "available": "-1",
        "expected_power": "-1",
        "cost_expected_power": "-1",
    }

    turbojet_Powerplant = Turbojet_Powerplant(**turbojet_Powerplant_data)
    print(turbojet_Powerplant.name)
    # > gasfiredbig1
    print(turbojet_Powerplant.dict())
    """
    {
      'name': 'tj1',
      'type': 'turbojet',
      'efficiency': '0.3',
      'pmin': '0',
      'pmax': '16',
      'available": '-1',
      'fuel_conditions': ['Kerosine_Fuel'],
      'expected_power': '-1',
      'cost_expected_power': '-1'
    }
    """

    windturbine_Powerplant_data = {
        "name": "windpark1",
        "efficiency": "1",
        "pmin": "0",
        "pmax": "150",
        "available": "-1",
        "expected_power": "-1",
        "cost_expected_power": "-1",
    }

    windturbine_Powerplant = Windturbine_Powerplant(**windturbine_Powerplant_data)
    print(windturbine_Powerplant.name)
    # > windpark1
    print(windturbine_Powerplant.dict())
    """
    {
      'name': 'windpark1',
      'type': 'windturbine',
      'efficiency': '1',
      'pmin': '0',
      'pmax': '150',
      'fuel_conditions': ['Wind_Fuel'],
      'expected_power': '-1',
      'cost_expected_power': '-1'
    }
    """
